# MyMonee - Duitkuu
is an app that develops for managing your money and could make your wishlist to buy in the journey of life.

## Launchscreen
![launchscreen.png](launchscreen.png)


## Homepage and Impian
![homepage.png](homepage.png) ![impian.png](impian.png)

## Detail History and Add Impian
![detail_history.png](detail_history.png) ![add_impian.png](add_impian.png)

## Features
* [x] History : CRUD
* [x] Impian : CRUD
* [x] Profile

## Requirements
- iOS 12.1
- Xcode 12.4

## Usage Details
In this app we used MVC (Model, View, and Controller) for the design pattern.

## Meta
Dody Setiawan - Digital Innovation Program Batch 1 by Bank Mandiri
dodysetiawan6690@gmail.com
