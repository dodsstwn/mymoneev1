//
//  NetworkService.swift
//  Duitkuu
//
//  Created by Dodsstwn on 20/05/21.
//

import Foundation

class NetworkService {
    let url: String = "https://60a5e4d6c0c1fd00175f49bd.mockapi.io/api/v1/transaction"
    
    func loadTrasactionList(completion: @escaping (_ transactions: [Spending]) -> ()) {
        let components = URLComponents(string: self.url)
        let request = URLRequest(url: (components?.url)!)
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            if let data = data{
                let decoder = JSONDecoder()
                if let spendingTransaction = try? decoder.decode(TransactionResponse.self, from: data) as
                    TransactionResponse {
                    completion(spendingTransaction.results ?? [])
                }
            }
//            Print data yang telah di load dari API
//            print(String(data: data!, encoding: String.Encoding.utf8)!)
        }
        task.resume()
    }
    
    func postMethod(id: String, spendingName: String, spendingPrice: Int, status: Bool, date: String) {
        guard let url = URL(string: "https://60a5e4d6c0c1fd00175f49bd.mockapi.io/api/v1/transaction") else {
            print("Error: cannot create URL")
            return
        }
        
        // Create model
        struct UploadData: Codable {
            var id: String?
            var spendingName: String?
            var spendingPrice: Int?
            var status: Bool = false
            var date: String?
        }
        
        // Add data to the model
        let uploadDataModel = UploadData(id: id, spendingName: spendingName, spendingPrice: spendingPrice, status: status, date: date)
        
        // Convert model to JSON data
        guard let jsonData = try? JSONEncoder().encode(uploadDataModel) else {
            print("Error: Trying to convert model to JSON data")
            return
        }
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type") // the request is JSON
        request.setValue("application/json", forHTTPHeaderField: "Accept") // the response expected to be in JSON format
        request.httpBody = jsonData
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling POST")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    return
                }
                guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                    print("Error: Couldn't print JSON in String")
                    return
                }
                
                print(prettyPrintedJson)
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }.resume()
    }
    
    func putMethod(id: String, spendingName: String, spendingPrice: Int, status: Bool, date: String) {
        guard let url = URL(string: "https://60a5e4d6c0c1fd00175f49bd.mockapi.io/api/v1/transaction/\(id)") else {
                print("Error: cannot create URL")
                return
            }
            
            // Create model
            struct UploadData: Codable {
                var id: String?
                var spendingName: String?
                var spendingPrice: Int?
                var status: Bool = false
                var date: String?
            }
            
            // Add data to the model
            let uploadDataModel = UploadData(id: id, spendingName: spendingName, spendingPrice: spendingPrice, status: status, date: date)
            
            // Convert model to JSON data
            guard let jsonData = try? JSONEncoder().encode(uploadDataModel) else {
                print("Error: Trying to convert model to JSON data")
                return
            }
            
            // Create the request
            var request = URLRequest(url: url)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    print("Error: error calling PUT")
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Error: Did not receive data")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    return
                }
                do {
                    guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                        print("Error: Cannot convert data to JSON object")
                        return
                    }
                    guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                        print("Error: Cannot convert JSON object to Pretty JSON data")
                        return
                    }
                    guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                        print("Error: Could print JSON in String")
                        return
                    }
                    
                    print(prettyPrintedJson)
                } catch {
                    print("Error: Trying to convert JSON data to string")
                    return
                }
            }.resume()
    }
    
    func deleteMethod(id: String) {
        guard let url = URL(string: "https://60a5e4d6c0c1fd00175f49bd.mockapi.io/api/v1/transaction/\(id)") else {
                    print("Error: cannot create URL")
                    return
                }
                // Create the request
                var request = URLRequest(url: url)
                request.httpMethod = "DELETE"
                URLSession.shared.dataTask(with: request) { data, response, error in
                    guard error == nil else {
                        print("Error: error calling DELETE")
                        print(error!)
                        return
                    }
                    guard let data = data else {
                        print("Error: Did not receive data")
                        return
                    }
                    guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                        print("Error: HTTP request failed")
                        return
                    }
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                            print("Error: Could print JSON in String")
                            return
                        }
                        
                        print(prettyPrintedJson)
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                }.resume()
    }
}
