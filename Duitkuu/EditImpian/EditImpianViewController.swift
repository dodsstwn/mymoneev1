//
//  EditImpianViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 17/05/21.
//

import UIKit

class EditImpianViewController: UIViewController {

    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var titleEditImpian: UITextField!
    @IBOutlet weak var targetEditTextField: UITextField!
    @IBOutlet weak var pencapaianTextField: UITextField!
    
    @IBAction func updateButton(_ sender: Any) {
        updateImpian()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        deleteImpian()
        let backToImpian = ImpianViewController(nibName: String(describing: ImpianViewController.self), bundle: nil)
        
        navigationController?.pushViewController(backToImpian, animated: true)
    }
    
    var judul: String?
    var target: Int?
    var pencapaian: Int?
    var indexRow: Int?
    var id: Int?
    var progressValue: Float?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleEditImpian.attributedText = NSAttributedString(string: judul!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
        targetEditTextField.attributedText = NSAttributedString(string: String(target!), attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
        pencapaianTextField.attributedText = NSAttributedString(string: String(pencapaian!), attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
    }
    
    func deleteImpian() {
        impian.remove(at: indexRow!)
    }
    
    func updateImpian() {
        impian[indexRow!] = Impian(id: id, impianName: titleEditImpian.text, impianPrice: Int(targetEditTextField.text!), pencapaian: Int(pencapaianTextField.text!), progressStatus: Double(progressValue!))
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
