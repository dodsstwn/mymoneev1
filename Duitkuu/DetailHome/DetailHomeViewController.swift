//
//  DetailHomeViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import UIKit

class DetailHomeViewController: UIViewController {
    
    
    @IBOutlet weak var titleTransaction: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var typeTransaction: UILabel!
    @IBOutlet weak var nominalTransaction: UILabel!
    
    @IBAction func editButton(_ sender: Any) {
        let editHistoryController = EditHistoryViewController(nibName: String(describing: EditHistoryViewController.self), bundle: nil)
        
        editHistoryController.judul = titleTransaction.text!
        editHistoryController.nominal = nominal
        editHistoryController.type = typeTransaction.text!
        editHistoryController.indexRow = indexRow
        editHistoryController.id = id
        editHistoryController.dateEdit = dateTime
        editHistoryController.status = status
        
        navigationController?.pushViewController(editHistoryController, animated: true)
    }
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    var judul: String = ""
    var type: String = ""
    var nominal: Int?
    var image: String = ""
    var id: Int?
    var dateTime: String = ""
    var indexRow: Int?
    var status: Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTransaction.text = judul
        statusImage.image = UIImage(named: "\(image)")
        idLabel.text = String(id ?? 0)
        dateLabel.text = dateTime
        
        // Rupiah Formatter
        if let formattedTipAmount = rupiahFormatter().string(from: nominal! as NSNumber) {
            nominalTransaction.text = "Rp \(formattedTipAmount)"
        }
        
        if type == "Pengeluaran" {
            nominalTransaction.textColor = .systemRed
            typeTransaction.text = "Pengeluaran"
        } else {
            nominalTransaction.textColor = .systemGreen
            typeTransaction.text = "Pemasukan"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}

extension DetailHomeViewController: ConvertRupiah {
    func rupiahFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal

        return formatter
    }
}
