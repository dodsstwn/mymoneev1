//
//  EditProfileViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 18/05/21.
//

import UIKit

class EditProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profileNameTextField: UITextField!
    @IBAction func choosePhoto(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        self.present(image, animated: true) {
        }
    }
    @IBAction func saveProfile(_ sender: Any) {
        updateProfileName()
        UserDefaults.standard.set(try? PropertyListEncoder().encode(account), forKey: "saveName")
        let backToProfile = ProfileViewController(nibName: String(describing: ProfileViewController.self), bundle: nil)
        navigationController?.pushViewController(backToProfile, animated: true)
    }
        
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var genderType: UILabel!
    @IBOutlet weak var emailUser: UILabel!
    @IBOutlet weak var phoneUser: UILabel!
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profilePicture.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make UIImage to Circle
        profilePicture.layer.masksToBounds = false
        profilePicture.layer.cornerRadius = profilePicture.frame.height/2
        profilePicture.clipsToBounds = true
        
        // profileName textField
        profileNameTextField.text = account[0].name
        
    }
    
    func updateProfileName() {
        account[0] = Account(id: account[0].id, name: profileNameTextField.text, gender: account[0].gender, email: account[0].email, phoneNumber: account[0].phoneNumber, statusWallet: account[0].statusWallet)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        profileName.text = account[0].name
        genderType.text = account[0].gender
        emailUser.text = account[0].email
        phoneUser.text = account[0].phoneNumber
    }
    
    // Code for remove the top Navigation Bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }


}
