//
//  MainViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 12/05/21.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageNotFound: UIImageView!
    @IBOutlet weak var textNotFound: UITextView!
    @IBOutlet weak var balanceLable: UILabel!
    @IBOutlet weak var uangMasuk: UILabel!
    @IBOutlet weak var uangKeluar: UILabel!
    @IBOutlet weak var addButtonBottom: UIButton!
    @IBOutlet weak var loadingLabel: UIActivityIndicatorView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBAction func addButtonBottom(_ sender: Any) {
        let addHistoryController = AddHistoryViewController(nibName: String(describing: AddHistoryViewController.self), bundle: nil)
        navigationController?.pushViewController(addHistoryController, animated: true)
    }
    
    @IBAction func addHistory(_ sender: Any) {
        print("> Add History Button pressed")
        let addHistoryController = AddHistoryViewController(nibName: String(describing: AddHistoryViewController.self), bundle: nil)
        navigationController?.pushViewController(addHistoryController, animated: true)
    }
    
    var service: NetworkService = NetworkService()
    var transactionList: [Spending] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    func loadData() {
        loadingLabel.startAnimating()
        viewBottom.isHidden = false
        service.loadTrasactionList { transaction in
            DispatchQueue.main.async {
                self.transactionList = transaction
                self.totalBalance()
                let balanceUser: Int = wallet[0].balance!
                let incomeUser: Int = wallet[0].income!
                let outcomeUser: Int = wallet[0].outcome!

                if let formattedTipAmount = self.rupiahFormatter().string(from: balanceUser as NSNumber) {
                    self.balanceLable.text = "Rp \(formattedTipAmount)"
                }
                if let formattedTipAmount = self.rupiahFormatter().string(from: incomeUser as NSNumber) {
                    self.uangMasuk.text = "Rp \(formattedTipAmount)"
                }
                if let formattedTipAmount = self.rupiahFormatter().string(from: outcomeUser as NSNumber) {
                    self.uangKeluar.text = "Rp \(formattedTipAmount)"
                }
                self.loadingLabel.stopAnimating()
                self.loadingLabel.isHidden = true
                self.viewBottom.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        let uiNib = UINib(nibName: String(describing: HomeTableViewCell.self), bundle: nil)
        tableView.register(uiNib, forCellReuseIdentifier: String(describing: HomeTableViewCell.self))
        
        self.loadData()
        // Time Management
        let timeHour = TimeManagement.init().getHour()
        
        if timeHour >= 0 && timeHour <= 10 {
            timeText.text = "Selamat Pagi,"
        } else if timeHour >= 11 && timeHour <= 14 {
            timeText.text = "Selamat Siang,"
        } else if timeHour >= 15 && timeHour <= 18 {
            timeText.text = "Selamat Sore,"
        } else if timeHour >= 19 && timeHour <= 23 {
            timeText.text = "Selamat Malam,"
        }
        
    }
    
    func totalBalance() {
        var totalPemasukan: Int = 0
        var totalPengeluaran: Int = 0
        transactionList.forEach {
            if $0.status {
                totalPemasukan += $0.spendingPrice!
            } else {
                totalPengeluaran += $0.spendingPrice!
            }
        }
        wallet[0].income = totalPemasukan
        wallet[0].outcome = totalPengeluaran
        wallet[0].balance = totalPemasukan - totalPengeluaran
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        nameText.text = account[0].name
        tableView.reloadData()
        
        // Balance Label
        totalBalance()
        let balanceUser: Int = wallet[0].balance!
        let incomeUser: Int = wallet[0].income!
        let outcomeUser: Int = wallet[0].outcome!

        if let formattedTipAmount = rupiahFormatter().string(from: balanceUser as NSNumber) {
            balanceLable.text = "Rp \(formattedTipAmount)"
        }
        if let formattedTipAmount = rupiahFormatter().string(from: incomeUser as NSNumber) {
            uangMasuk.text = "Rp \(formattedTipAmount)"
        }
        if let formattedTipAmount = rupiahFormatter().string(from: outcomeUser as NSNumber) {
            uangKeluar.text = "Rp \(formattedTipAmount)"
        }
        
        self.loadData()
    }
    
    // Code for remove the top Navigation Bar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Empty Data Handling
        if transactionList.count <= 0 {
            tableView.isHidden = true
            addButtonBottom.isHidden = false
            imageNotFound.isHidden = false
            textNotFound.isHidden = false
        } else {
            tableView.isHidden = false
            addButtonBottom.isHidden = true
            imageNotFound.isHidden = true
            textNotFound.isHidden = true
        }
        return transactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeTableViewCell.self), for: indexPath) as! HomeTableViewCell

        cell.titleTransaction.text = transactionList[indexPath.row].spendingName
        cell.dateTransaction.text = transactionList[indexPath.row].date
        cell.nominalTransaction.text = "\(transactionList[indexPath.row].spendingPrice!)"
        
        if transactionList[indexPath.row].status {
            let incomeUser: Int = transactionList[indexPath.row].spendingPrice!
            if let formattedTipAmount = rupiahFormatter().string(from: incomeUser as NSNumber) {
                cell.nominalTransaction.text = "Rp \(formattedTipAmount)"
            }
            cell.imageStatus.image = UIImage(named: "up-green")
            cell.nominalTransaction.textColor = .systemGreen
        } else {
            let outcomeUser: Int = transactionList[indexPath.row].spendingPrice!
            cell.imageStatus.image = UIImage(named: "down-red")
            if let formattedTipAmount = rupiahFormatter().string(from: outcomeUser as NSNumber) {
                cell.nominalTransaction.text = "Rp \(formattedTipAmount)"
            }
            cell.nominalTransaction.textColor = .systemRed
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Click On Each Cell
        let detailHomeTransaction = DetailHomeViewController(nibName: String(describing: DetailHomeViewController.self), bundle: nil)
        
        detailHomeTransaction.indexRow = indexPath.row
        detailHomeTransaction.judul = transactionList[indexPath.row].spendingName ?? ""
        detailHomeTransaction.nominal = transactionList[indexPath.row].spendingPrice
        detailHomeTransaction.id = Int(transactionList[indexPath.row].id!)
        detailHomeTransaction.dateTime = transactionList[indexPath.row].date ?? ""
        detailHomeTransaction.status = transactionList[indexPath.row].status
        
        if transactionList[indexPath.row].status {
            detailHomeTransaction.type = "Pemasukan"
            detailHomeTransaction.image = "up-green"
        } else {
            detailHomeTransaction.type = "Pengeluaran"
            detailHomeTransaction.image = "down-red"
        }
        
        // Move to the other View Controller
        navigationController?.pushViewController(detailHomeTransaction, animated: true)
    }
}

extension MainViewController: ConvertRupiah {
    func rupiahFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        return formatter
    }
}
