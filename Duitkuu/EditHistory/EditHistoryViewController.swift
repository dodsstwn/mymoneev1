//
//  EditHistoryViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import UIKit

class EditHistoryViewController: UIViewController {
    
    @IBAction func deleteButton(_ sender: Any) {
        deleteHistory()
//        let backToHistory = MainViewController(nibName: String(describing: MainViewController.self), bundle: nil)
//        navigationController?.pushViewController(backToHistory, animated: true)
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func updateHistory(_ sender: Any) {
        updateHistory()
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func selectTypeButton(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            status = true
        } else {
            status = false
        }
    }
    
    @IBOutlet weak var titleEditTextField: UITextField!
    @IBOutlet weak var nominalEditTextField: UITextField!
    @IBOutlet weak var selectTypeButton: UISegmentedControl!
    
    var service: NetworkService = NetworkService()
    var judul: String = ""
    var nominal: Int?
    var type: String = ""
    var indexRow: Int?
    var id: Int?
    var status: Bool?
    var dateEdit: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleEditTextField.attributedText = NSAttributedString(string: judul, attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])

        nominalEditTextField.attributedText = NSAttributedString(string: String(nominal!), attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])

    }
    
    func putData(){
        service.putMethod(id: String(id!), spendingName: titleEditTextField.text!, spendingPrice: Int(nominalEditTextField.text!)!, status: status!, date: timeConverter())
    }
    
    func deleteData(){
        service.deleteMethod(id: String(id!))
    }
    
    func updateHistory() {
        self.putData()
//        transactionList[indexRow!] = Spending(id: String(id!), spendingName: titleEditTextField.text, spendingPrice: Int(nominalEditTextField.text!), status: status!, date: dateEdit)
    }

    func deleteHistory() {
        self.deleteData()
//        transactionList.remove(at: indexRow!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }


}
