//
//  HomeTableViewCell.swift
//  Duitkuu
//
//  Created by Dodsstwn on 13/05/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var titleTransaction: UILabel!
    @IBOutlet weak var dateTransaction: UILabel!
    @IBOutlet weak var nominalTransaction: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func showData(transaction: Spending) {
        titleTransaction.text = transaction.spendingName
        nominalTransaction.text = String(transaction.spendingPrice!)
        dateTransaction.text = transaction.date
    }
    
}
