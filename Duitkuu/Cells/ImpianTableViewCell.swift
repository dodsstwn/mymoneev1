//
//  ImpianTableViewCell.swift
//  Duitkuu
//
//  Created by Dodsstwn on 14/05/21.
//

import UIKit

protocol ImpianProtocol {
    func doneImpian(indexAt: Int)
    func deleteImpian(indexAt: Int)
}

class ImpianTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleImpian: UILabel!
    @IBOutlet weak var progressStatus: UIProgressView!
    @IBOutlet weak var nominalImpian: UILabel!
    @IBOutlet weak var finishImpian: UIButton!
    
    @IBAction func doneButton(_ sender: Any) {
        // Green
        delegate?.doneImpian(indexAt: indexAt!)
    }
    
    @IBAction func trashButton(_ sender: Any) {
        // Red
        delegate?.deleteImpian(indexAt: indexAt!)
    }
    
    var delegate: ImpianProtocol?
    var indexAt: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
