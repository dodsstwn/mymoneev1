//
//  ProfileViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 14/05/21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var statusWallet: UITextView!
    @IBOutlet weak var profileStaticPic: UIImageView!
    
    @IBAction func profileEdit(_ sender: Any) {
        let profileEditPage = EditProfileViewController(nibName: String(describing: EditProfileViewController.self), bundle: nil)
        navigationController?.pushViewController(profileEditPage, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .systemGray6
        // Do any additional setup after loading the view.
        
        if account[0].statusWallet == "Bagus" {
            statusWallet.text = "Bagus! Pengeluaranmu lebih sedikit daripada penghasilanmu"
        } else if account[0].statusWallet == "Buruk" {
            statusWallet.text = "Wah! Sepertinya kondisi keuanganmu sedang tidak bagus"
        }
        
        if let savedData = UserDefaults.standard.value(forKey: "savedArray") as? Data {
            let _account = try? PropertyListDecoder().decode(Array<Account>.self, from: savedData)
            account = _account ?? []
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        accountName.text = account[0].name
        
        if let savedData = UserDefaults.standard.value(forKey: "savedArray") as? Data {
            let _account = try? PropertyListDecoder().decode(Array<Account>.self, from: savedData)
            account = _account ?? []
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
