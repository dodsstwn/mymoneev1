//
//  DetailImpianViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import UIKit

class DetailImpianViewController: UIViewController {

    @IBOutlet weak var titleImpian: UILabel!
    @IBOutlet weak var nominalImpian: UILabel!
    @IBOutlet weak var percentProgress: UILabel!
    @IBOutlet weak var detailNominal: UILabel!
    @IBOutlet weak var progressImpian: UIProgressView!
    
    @IBAction func editButton(_ sender: Any) {
        let editImpianPage = EditImpianViewController(nibName: String(describing: EditImpianViewController.self), bundle: nil)
        
        editImpianPage.judul = titleImpian.text!
        editImpianPage.target = detail
        editImpianPage.pencapaian = pencapaian
        editImpianPage.indexRow = indexRow
        editImpianPage.id = id
        editImpianPage.progressValue = progressValue
        
        navigationController?.pushViewController(editImpianPage, animated: true)
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    var judul: String = ""
    var percent: String = ""
    var nominal: String = ""
    var detail: Int?
    var pencapaian: Int?
    var progressValue: Float = 0.0
    var indexRow: Int?
    var id: Int?
    
    @IBAction func confirmButton(_ sender: Any) {
        impian[indexRow!] = Impian(id: id, impianName: judul, impianPrice: detail, pencapaian: detail, progressStatus: 1)
        showAlertDone()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let impianUser: Int = detail!
        let pencapaianUser: Int = pencapaian!
        var impianUserStr: String = ""
        var pencapaianUserStr: String = ""
        
        if let formattedTipAmount = rupiahFormatter().string(from: impianUser as NSNumber) {
            impianUserStr = "\(formattedTipAmount)"
        }
        
        if let formattedTipAmount = rupiahFormatter().string(from: pencapaianUser as NSNumber) {
            pencapaianUserStr = "\(formattedTipAmount)"
        }
        
        titleImpian.text = judul
        nominalImpian.text = "Rp \(impianUserStr)"
        detailNominal.text = "IDR \(pencapaianUserStr) / IDR \(impianUserStr)"
        progressImpian.progress = Float(Double(pencapaian!) / Double(detail!))
        percentProgress.text = "\(round(progressImpian.progress*100))%"
    }
    
    func showAlertDone() {
        // Done Capaian's Alert
        
        let alert = UIAlertController(title: "Selamat!", message: "Impianmu telah tercapai!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in print("> Tapped OK")}))
        
        self.present(alert, animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension DetailImpianViewController: ConvertRupiah {
    func rupiahFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        return formatter
    }
}
