//
//  ImpianViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 14/05/21.
//

import UIKit

class ImpianViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageNotFound: UIImageView!
    @IBOutlet weak var textNotFound: UITextView!
    @IBOutlet weak var addButtonBottom: UIButton!
    @IBAction func addButtonBottom(_ sender: Any) {
        let addImpianPage = AddImpianViewController(nibName: String(describing: AddImpianViewController.self), bundle: nil)
        navigationController?.pushViewController(addImpianPage, animated: true)
    }
    @IBAction func addButton(_ sender: Any) {
        let addImpianPage = AddImpianViewController(nibName: String(describing: AddImpianViewController.self), bundle: nil)
        navigationController?.pushViewController(addImpianPage, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .systemGray6
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let uiNib = UINib(nibName: String(describing: ImpianTableViewCell.self), bundle: nil)
        tableView.register(uiNib, forCellReuseIdentifier: String(describing: ImpianTableViewCell.self))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension ImpianViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if impian.count <= 0 {
            tableView.isHidden = true
            print("> Data empty!")
            addButtonBottom.isHidden = false
            imageNotFound.isHidden = false
            textNotFound.isHidden = false
        } else {
            tableView.isHidden = false
            addButtonBottom.isHidden = true
            imageNotFound.isHidden = true
            textNotFound.isHidden = true
        }
        return impian.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImpianTableViewCell.self), for: indexPath) as! ImpianTableViewCell
        
        let pencapaianUser: Int = impian[indexPath.row].pencapaian!
        let impianUser: Int = impian[indexPath.row].impianPrice!
        var pencapaianUserStr: String = ""
        var impianUserStr: String = ""
        
        if let formattedTipAmount = rupiahFormatter().string(from: pencapaianUser as NSNumber) {
            pencapaianUserStr = "\(formattedTipAmount)"
        }
        if let formattedTipAmount = rupiahFormatter().string(from: impianUser as NSNumber) {
            impianUserStr = "\(formattedTipAmount)"
        }
        
        cell.delegate = self
        cell.indexAt = indexPath.row
        cell.titleImpian.text = impian[indexPath.row].impianName
        cell.nominalImpian.text = "IDR \(pencapaianUserStr) / IDR \(impianUserStr) "
        cell.progressStatus.progress = Float(Double(impian[indexPath.row].pencapaian!) / Double(impian[indexPath.row].impianPrice!))
        
        if cell.progressStatus.progress >= 1.0 {
            cell.finishImpian.isEnabled = true
        } else {
            cell.finishImpian.isEnabled = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailImpian = DetailImpianViewController(nibName: String(describing: DetailImpianViewController.self), bundle: nil)
        
        detailImpian.judul = impian[indexPath.row].impianName!
        detailImpian.detail = impian[indexPath.row].impianPrice!
        detailImpian.percent = String((impian[indexPath.row].pencapaian! / impian[indexPath.row].impianPrice!)*100)
        detailImpian.progressValue = Float(impian[indexPath.row].progressStatus)
        detailImpian.indexRow = indexPath.row
        detailImpian.id = impian[indexPath.row].id!
        detailImpian.pencapaian = impian[indexPath.row].pencapaian!
        
        navigationController?.pushViewController(detailImpian, animated: true)
    }
}

extension ImpianViewController: ConvertRupiah {
    func rupiahFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        return formatter
    }
}

extension ImpianViewController: ImpianProtocol {
    func doneImpian(indexAt: Int) {
        let alert = UIAlertController(title: "Pesan Konfirmasi", message: "Apakah anda yakin untuk menghapus impian anda ini karena sudah selesai?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in impian.remove(at: indexAt); self.tableView.reloadData(); print("> Impian telah terhapus")}))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {action in print("> Tapped Cancel")}))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteImpian(indexAt: Int) {
        let alert = UIAlertController(title: "Pesan Konfirmasi", message: "Apakah anda yakin untuk menghapus impian anda ini?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in impian.remove(at: indexAt); self.tableView.reloadData(); print("> Impian telah terhapus")}))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {action in print("> Tapped Cancel")}))
        
        self.present(alert, animated: true, completion: nil)
    }
}
