//
//  KonversiRupiah.swift
//  Duitkuu
//
//  Created by Dodsstwn on 19/05/21.
//

import Foundation

extension NumberFormatter {
    func rupiahFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        return formatter
    }
}
