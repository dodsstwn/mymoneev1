//
//  Wallet.swift
//  Duitkuu
//
//  Created by Dodsstwn on 17/05/21.
//

import Foundation

struct Wallet {
    var id: Int?
    var owner: String?
    var balance: Int?
    var income: Int?
    var outcome: Int?
}

var wallet: [Wallet] = [Wallet(id: 1, owner: account[0].name, balance: 0, income: 0, outcome: 0)]
