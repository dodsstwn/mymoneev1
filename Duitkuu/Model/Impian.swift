//
//  Impian.swift
//  Duitkuu
//
//  Created by Dodsstwn on 14/05/21.
//

import Foundation

struct Impian {
    var id: Int?
    var impianName: String?
    var impianPrice: Int?
    var pencapaian: Int?
    var progressStatus: Double = 0.0
}

var impian: [Impian] = [Impian(id: 1, impianName: "Mazda 3 2020", impianPrice: 400000000, pencapaian: 20000000, progressStatus: 0.4), Impian(id: 2, impianName: "Rumah di Foresta Ultimo", impianPrice: 1000000000, pencapaian: 300000000, progressStatus: 0.4)]
