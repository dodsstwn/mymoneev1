//
//  Spending.swift
//  Duitkuu
//
//  Created by Dodsstwn on 13/05/21.
//

import Foundation


struct TransactionResponse: Codable {
    var results: [Spending]?
}

struct Spending: Codable {
    var id: String?
    var spendingName: String?
    var spendingPrice: Int?
    var status: Bool = false
    var date: String?
}

//var spending: [Spending] = [Spending(id: 1, spendingName: "Gajian", spendingPrice: 5000000, status: true, date: "16-07-2020"), Spending(id: 2, spendingName: "Top Up OVO", spendingPrice: 200000, status: false, date: "23-06-1998")]

//var spending: [Spending] = []

// status -> false => Pengeluaran
// status -> true => Pemasukan
