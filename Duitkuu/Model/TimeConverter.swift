//
//  TimeConverter.swift
//  Duitkuu
//
//  Created by Dodsstwn on 20/05/21.
//

import Foundation

func timeConverter() -> String {
    let currentDate = TimeManagement.init()
    let year = String(currentDate.getYear())
    let month = String(currentDate.getMonth())
    let day = String(currentDate.getDay())
    let hour = String(currentDate.getHour())
    let minute = String(currentDate.getMinute())
    var minuteStr = ""
    var monthName = ""
    
    if month == "1" {
        monthName = "Jan"
    } else if month == "2" {
        monthName = "Feb"
    } else if month == "3" {
        monthName = "Mar"
    } else if month == "4" {
        monthName = "Apr"
    } else if month == "5" {
        monthName = "Mei"
    } else if month == "6" {
        monthName = "Jun"
    } else if month == "7" {
        monthName = "Jul"
    } else if month == "8" {
        monthName = "Ags"
    } else if month == "9" {
        monthName = "Sep"
    } else if month == "10" {
        monthName = "Okt"
    } else if month == "11" {
        monthName = "Nov"
    } else if month == "12" {
        monthName = "Des"
    }
    
    if Int(minute)! <= 9 {
        minuteStr = "0\(minute)"
    } else {
        minuteStr = minute
    }
    
    return "\(day) \(monthName) \(year) - \(hour).\(minuteStr)"
}
