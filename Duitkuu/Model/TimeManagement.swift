//
//  TimeManagement.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import Foundation

class TimeManagement {
    
    let currentDateTime = Date()
    let userCalendar = Calendar.current
    let requestedComponents: Set<Calendar.Component> = [
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second
    ]
    
    func getHour() -> Int {
        let dateTimeComponents =  userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        return dateTimeComponents.hour!
    }
    
    func getMinute() -> Int {
        let dateTimeComponents =  userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        return dateTimeComponents.minute!
    }
    
    func getYear() -> Int {
        let dateTimeComponents =  userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        return dateTimeComponents.year!
    }
    
    func getMonth() -> Int {
        let dateTimeComponents =  userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        return dateTimeComponents.month!
    }
    
    func getDay() -> Int {
        let dateTimeComponents =  userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        return dateTimeComponents.day!
    }
    
}
