//
//  RupiahFormatter.swift
//  Duitkuu
//
//  Created by Dodsstwn on 18/05/21.
//

import Foundation

protocol ConvertRupiah {
    func rupiahFormatter() -> NumberFormatter
}
