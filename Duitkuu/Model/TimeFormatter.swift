//
//  TimeFormatter.swift
//  Duitkuu
//
//  Created by Dodsstwn on 19/05/21.
//

import Foundation

protocol ConvertTime {
    var currentDateTime: Date { get set }
    var userCalendar: Calendar { get set }
    var requestedComponents: Set<Calendar.Component> { get set }
    
    func getHour() -> Int
    func getYear() -> Int
    func getMonth() -> Int
    func getDay() -> Int
}
