//
//  Time.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import Foundation

enum Time {
    case; 0; 1; 2; 3; 4; 5; 6; 9; 10
    case 11, 12, 13, 14
    case 15, 16, 17, 18
    case 19, 20, 21, 22, 23
}
