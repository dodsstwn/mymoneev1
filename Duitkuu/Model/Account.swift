//
//  Account.swift
//  Duitkuu
//
//  Created by Dodsstwn on 15/05/21.
//

import Foundation

struct Account: Codable {
    var id: Int?
    var name: String?
    var gender: String?
    var email: String?
    var phoneNumber: String?
    var statusWallet: String?
}

var account = [Account(id: 1, name: "Satria Yudha Perkasa", gender: "M", email: "satrio@gmail.com", phoneNumber: "081115024999", statusWallet: "Bagus")]
