//
//  AddHistoryViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 15/05/21.
//

import UIKit

class AddHistoryViewController: UIViewController {
    
    // Global Variable
    let randomInt = Int.random(in: 1..<2000)
    var spendingStatus: Bool = false
    let uuid = UUID().uuidString
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var nominalTextField: UITextField!
    
    @IBAction func selectTypeHistory(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            spendingStatus = true
        } else {
            spendingStatus = false
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
//        transactionList.insert(Spending(id: String(randomInt), spendingName: titleTextField.text, spendingPrice: Int(nominalTextField.text!), status: spendingStatus, date: "\(day)-\(month)-\(year)"), at: 0)
        self.postData()
        navigationController?.popViewController(animated: true)
    }
    
    var service: NetworkService = NetworkService()
    
    func postData() {
        
        service.postMethod(id: uuid, spendingName: titleTextField.text!, spendingPrice: Int(nominalTextField.text!)!, status: spendingStatus, date: timeConverter())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Placeholder
        titleTextField.attributedPlaceholder = NSAttributedString(string:"Gaji", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
        nominalTextField.attributedPlaceholder = NSAttributedString(string:"1.250.000", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
}
