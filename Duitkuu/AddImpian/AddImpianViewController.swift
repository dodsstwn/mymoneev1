//
//  AddImpianViewController.swift
//  Duitkuu
//
//  Created by Dodsstwn on 16/05/21.
//

import UIKit

class AddImpianViewController: UIViewController {
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var judulTextField: UITextField!
    @IBOutlet weak var targetTextField: UITextField!
    @IBOutlet weak var pencapaianTextField: UITextField!
    
    @IBAction func saveButton(_ sender: Any) {
        let randomInt = Int.random(in: 1..<2000)
        
        impian.insert(Impian(id: randomInt, impianName: judulTextField.text, impianPrice: Int(targetTextField.text!), pencapaian: Int(pencapaianTextField.text!), progressStatus: 0.2), at: 0)
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Placeholder
        judulTextField.attributedPlaceholder = NSAttributedString(string: "Membeli Airpods Baru", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
        targetTextField.attributedPlaceholder = NSAttributedString(string: "1.500.000", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
        pencapaianTextField.attributedPlaceholder = NSAttributedString(string: "500.000", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font :UIFont(name: "Poppins", size: 20)!])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}
